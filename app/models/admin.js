'use strict';

/**
	 Admin Model
**/
var crypto = require('crypto');

module.exports = function(sequelize, DataTypes)
{
    var Admin = sequelize.define('Admin',
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true
            },
            username: DataTypes.STRING,
            password: DataTypes.STRING,
            provider: DataTypes.STRING,
            salt: DataTypes.STRING,
            fname: DataTypes.STRING,
            lname: DataTypes.STRING,
            roles: DataTypes.STRING

        },
        {
            tableName: 'axi_admins',
            instanceMethods: {
                toJSON: function () {
                    var values = this.get();
                    delete values.password;
                    delete values.salt;
                    return values;
                },
                makeSalt: function() {
                    return crypto.randomBytes(16).toString('base64');
                },
                authenticate: function(plainText) {
                    console.log("Authenticate:",plainText);
                    console.log("BOOL",this.encryptPassword(plainText, this.salt) === this.password);
                    return this.encryptPassword(plainText, this.salt) === this.password;
                },
                encryptPassword: function(password, salt) {
                    try{
                        if (!password || !salt) {
                            return '';
                        }
                        salt = new Buffer(salt, 'base64');
                        console.log("Salt:",salt);
                        return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
                    }
                    catch(err)
                    {
                        console.log("Exception:",err);
                        process.exit(0);
                    }

                }
            }
        }
    );
    return Admin;
};
