'use strict';

/**
* Module dependencies.
*/
var passport = require('passport');

module.exports = function(app)
{
    var admins = require('../../app/controllers/admins');

    //Admin Routes
    app.get('/signout', admins.signout);
    app.get('/admins/me', admins.me);

    // Setting up the users api
    app.post('/admins', admins.create); //Signup

    app.post('/admins/session', passport.authenticate('local', { //Login
        failureRedirect: '/signin',
        failureFlash: true
    }), admins.session);

    // Finish with setting up the userId param
    app.param('id', admins.admin);
};
