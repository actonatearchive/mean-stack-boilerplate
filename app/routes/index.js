'use strict';

module.exports = function(app) {
// Home route
var index = require('../../app/controllers/index');
var admin = require('../../app/controllers/admins');
    app.get('/', admin.signin);
    app.get('/main/dashboard',admin.requiresLogin, index.render);
    app.get('/signin', index.render);
};
