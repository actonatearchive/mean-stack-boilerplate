'use strict';

/**
 * Module dependencies.
 */
var db = require('../../config/sequelize');

/**
 * Auth callback
 */
exports.authCallback = function(req, res, next) {
    res.redirect('/');
    console.log("AuthCallback()");
};
/**
 * Show login form
 */
exports.signin = function(req, res) {
    console.log("LoginRequest:",req);
    console.log("Exports: SignIn PAGE!");
    // res.render('signin', {
    //     title: 'Login',
    //     message: req.flash('error')
    // });
    res.redirect('/signin');

};

/**
 * Session
 */
exports.session = function(req, res) {
    console.log("SessionRequest:",req);
    console.log("Exports: SESSION");
    return res.send({status : 'success', message : 'User login successfully.'})
   // res.redirect('/');
};
/**
 * Create Admin
 */
exports.create = function(req, res, next) {
    var message = null;

    var admin = db.Admin.build(req.body);

    admin.provider = 'local';
    admin.salt = admin.makeSalt();
    admin.password = admin.encryptPassword(req.body.password, admin.salt);
    console.log('New User (local) : { id: ' + admin.id + ' username: ' + admin.username + ' }');

    admin.save().then(function(){
      req.login(admin, function(err){
        if(err) {
            return next(err);
        }
          return res.send({status : 'success', message : 'admin successfully Created.'})
       // res.redirect('/');
      });
    }).catch(function(err){
      res.render('admins/signup',{
          message: message,
          admin: admin
      });
    });
};

/**
 * Logout
 */
exports.signout = function(req, res) {

    // console.log("Exports: Signout Admin-",req.admin);
    // console.log("Exports: Signout User-",req.user);
    console.log('Logout: { id: ' + req.user.id + ', username: ' + req.user.username + '}');
    req.logout();
    return res.send({status : 'success', message : 'User logout successfully.'});
};

/**
 * Send User
 */
exports.me = function(req, res) {
    res.jsonp(req.user || null);
};

/**
 * Find admin by id
 */
exports.admin = function(req, res, next, id) {
    console.log("Exports: FindByAdminID()");
    db.Admin.find({where : { id: id }}).then(function(admin){
      if (!admin) {
          return next(new Error('Failed to load Admin ' + id));
      }
      req.profile = admin;
      next();
    }).catch(function(err){
      next(err);
    });
};


/**
 * Generic require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.status(401).send('User is not authorized');
    }
    next();
};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(req, res, next) {
    console.log("Request Profile :",req);
    if (req.profile.id !== req.user.id) {
      return res.status(401).send('User is not authorized');
    }
    next();
};
