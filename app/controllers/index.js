'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');


exports.render = function(req, res) {
    res.render('index', {
        admin: req.user ? JSON.stringify(req.user) : "null"
    });

    console.log("User:",JSON.stringify(req.user));
};
