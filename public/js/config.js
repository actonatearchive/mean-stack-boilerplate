'use strict';
//Setting up route
angular.module('mean').config(['$stateProvider','$urlRouterProvider', function($stateProvider,$urlRouterProvider) {

    $urlRouterProvider.otherwise(function($injector, $location){
        $injector.invoke(['$state', function($state) {
            $state.go('404');
        }]);
    });
    $stateProvider
    // .state('login', {
    //     url: '/login',
    //     controller: 'LoginController',
    //     templateUrl: 'views/login.html'
    //
    // })
    .state('main',{
        url : '/main',
        abstract: true,
        controller : 'HeaderController',
        templateUrl: 'views/header.html'
    })
    .state('main.dashboard',{
        url : '/dashboard',
        controller : 'IndexController',
        templateUrl: 'views/dashboard.html'
    })
        // .state('home',{
        //     url : '/',
        //     controller : 'IndexController',
        //     templateUrl: 'views/index.html'
        // })
        .state('SignIn',{
            url : '/signin',
            templateUrl: 'views/signin.html'
        })
        .state('SignUp',{
            url : '/signup',
            templateUrl: 'views/signup.html'
        })
        .state('404',{
            templateUrl: 'views/404.html'
        })
}
]);

//Setting HTML5 Location Mode
angular.module('mean').config(['$locationProvider', function ($locationProvider) {
    $locationProvider.html5Mode(true);

}]);
