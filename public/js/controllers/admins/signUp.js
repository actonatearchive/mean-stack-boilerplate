angular.module('mean.auth').controller('signUp', ['$scope', '$window', 'Global','$state', 'SignUp', function ($scope, $window, Global, $state, SignUp) {
    $scope.global = Global;


    $scope.signUp = function(admin) {

        var signUp = new SignUp({
            fname: admin.fname,
            lname: admin.lname,
            username : admin.username,
            password : admin.password
        });

        signUp.$save(function(response) {
            if(response.status === 'success'){
                $window.location.href = '/';
            }
            console.log("SignUp: Response: ", response);
        });
    };


}]);
