angular.module('mean.auth').controller('signIn', ['$scope', '$window', 'Global', '$state', 'LogIn', function ($scope, $window, Global, $state, LogIn) {
    $scope.global = Global;
    console.log("SignInController");


    $scope.signIn = function(admin) {

        var logIn = new LogIn({
            username: admin.username,
            password: admin.password
        });
        console.log("userInfo:",logIn);
        logIn.$save(function(response) {
            if(response.status === 'success'){
                $window.location.href = '/main/dashboard';
                // $state.go('main.dashboard');
            }
            console.log("Response: ",response);
        });
    };

}]);
