'use strict';

var passport = require('passport');
var _ = require('lodash');
// These are different types of authentication strategies that can be used with Passport.
var LocalStrategy = require('passport-local').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google').Strategy;
var config = require('./config');
var db = require('./sequelize');
var winston = require('./winston');

//Serialize sessions
passport.serializeUser(function(admin, done) {
  done(null, admin.id);
});

passport.deserializeUser(function(id, done) {
    db.Admin.find({where: {id: id}}).then(function(admin){
        if(!admin){
            winston.warn('Logged in user not in database, admin possibly deleted post-login');
            return done(null, false);
        }
        winston.info('Session: { id: ' + admin.id + ', username: ' + admin.username + ' }');
        // console.log(user)
        done(null, admin);
    }).catch(function(err){
        done(err, null);
    });
});

//Use local strategy
passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
  function(username, password, done) {

    db.Admin.find({ where: { username: username }}).then(function(admin) {
      if (!admin) {
        done(null, false, { message: 'Unknown user' });
      } else if (!admin.authenticate(password)) {
        done(null, false, { message: 'Invalid password'});
      } else {
        winston.info('Login (local) : { id: ' + admin.id + ', username: ' + admin.username + ' }');
        done(null, admin);
      }
    }).catch(function(err){
      done(err);
    });
  }
));

//    Use twitter strategy
// passport.use(new TwitterStrategy({
//         consumerKey: config.twitter.clientID,
//         consumerSecret: config.twitter.clientSecret,
//         callbackURL: config.twitter.callbackURL
//     },
//     function(token, tokenSecret, profile, done) {
//
//         db.User.find({where: {twitterUserId: profile.id}}).then(function(user){
//             if(!user){
//                 db.User.create({
//                     twitterUserId: profile.id,
//                     name: profile.displayName,
//                     username: profile.username,
//                     provider: 'twitter'
//                 }).then(function(u){
//                     winston.info('New User (twitter) : { id: ' + u.id + ', username: ' + u.username + ' }');
//                     done(null, u);
//                 });
//             } else {
//                 winston.info('Login (twitter) : { id: ' + user.id + ', username: ' + user.username + ' }');
//                 done(null, user);
//             }
//
//         }).catch(function(err){
//             done(err, null);
//         });
//     }
// ));
//
//
// // Use facebook strategy
// passport.use(new FacebookStrategy({
//         clientID: config.facebook.clientID,
//         clientSecret: config.facebook.clientSecret,
//         callbackURL: config.facebook.callbackURL
//     },
//     function(accessToken, refreshToken, profile, done) {
//
//         db.User.find({where : {facebookUserId: profile.id}}).then(function(user){
//             if(!user){
//                 db.User.create({
//                     name: profile.displayName,
//                     email: profile.emails[0].value,
//                     username: profile.username,
//                     provider: 'facebook',
//                     facebookUserId: profile.id
//                 }).then(function(u){
//                     winston.info('New User (facebook) : { id: ' + u.id + ', username: ' + u.username + ' }');
//                     done(null, u);
//                 })
//             } else {
//                 winston.info('Login (facebook) : { id: ' + user.id + ', username: ' + user.username + ' }');
//                 done(null, user);
//             }
//         }).catch(function(err){
//             done(err, null);
//         });
//     }
// ));
//
// //Use google strategy
// passport.use(new GoogleStrategy({
//     returnURL: config.google.callbackURL,
//     realm: config.google.realm
//   },
//   function(identifier, profile, done) {
//     console.log(identifier);
//     console.log(profile);
//
//     db.User.find({where: {openId: identifier}}).then(function(user){
//         if(!user){
//             db.User.create({
//                 name: profile.displayName,
//                 email: profile.emails[0].value,
//                 username: profile.displayName.replace(/ /g,''),
//                 openId: identifier,
//             }).then(function(u){
//                 winston.info('New User (google) : { id: ' + u.id + ', username: ' + u.username + ' }');
//                 done(null, u);
//             })
//         } else {
//             winston.info('Login (google) : { id: ' + user.id + ', username: ' + user.username + ' }');
//             done(null, user);
//         }
//     }).catch(function(err){
//         done(err, null);
//     });
//   }
// ));

module.exports = passport;
